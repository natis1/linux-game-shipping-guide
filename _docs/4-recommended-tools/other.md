---
title: "Other"
category: "Recommended Tools"
order: 6
---

## Docker

Docker is a software platform that allows for creating self-contained
contrainers of software, that bring with them all of their own libraries,
configuration files, and which communicate via defined channels. Docker
containers can be thought of as 'lightweight virtual machines'. Similar to how a
virtual machine virtualizes (removes the need to directly manage) server
hardware, containers virtualize the operating system of a server. With simple
commands, you can build, start, and stop containers.

Build once, run everywhere. Even on different operating systems.

Docker can be used to build containers for game servers, allowing you to, eg:
develop and test on a local Windows machine then deploy those same containers to
a Linux VPS.

Docker can also be used to build containers that contain your game engine, and
used to compile your game for any OS, from any OS. Using this, it is possible to
setup a continous integration pipeline, where your game is rebuilt with every
'git push' to your repository, and report any failure to compile.
