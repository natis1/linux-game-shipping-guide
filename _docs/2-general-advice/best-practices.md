---
title: "Best Practices"
category: "General Advice"
order: 3
---

## 64-bit

32-bit support is increasingly being phased out of the world of computing, and
this is true on Linux as well.
[The number of 32-bit Linux installations](https://www.gamingonlinux.com/users/statistics)
is now so low that there is no benefit to choosing 32-bit over 64bit, and 32-bit
builds are really unnecessary.

## Where To Store Game Data

The [XDG Base Directory Specification](https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html)
in simple terms describes where your software should place files on a user's
Linux PC. Much like on Windows, game configuration data and save game data
should be stored in the appropriate directories to avoid cluttering your user's
`/home` directory. Use `$XDG_DATA_HOME` for savegames and `$XDG_CONFIG_HOME` for
configuration files.

The game engine Unity has it's own behaviour for storing data, and stores all
your game data under `~/.config/unity3d/[Your Game Name]`. This doesn't adhere
to the XDG Base Directory Specification, but is still better than cluttering the
user's `/home` directory.

> Note: On Linux, the **~** character in a filepath refers to the user's 'home'
directory. For a user with the username `bob`, the filepath `~/file` would refer
to `/home/bob/file`.

## Locale Handling

_TODO. Need more information on this._
